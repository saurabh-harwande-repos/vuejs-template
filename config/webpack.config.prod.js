'use strict'

const { merge } = require('webpack-merge');
const common = require('./webpack.config.common.js');
const CompressionPlugin = require('compression-webpack-plugin');


module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    plugins: [
        new CompressionPlugin()
    ]
});