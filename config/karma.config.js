var webpackConfig = require('./webpack.config.dev.js');

module.exports = function(config) {
    config.set({
        basePath: ".",
        frameworks: [
            'mocha',
            'chai',
            'sinon'
        ],
        files: ['../src/test/**/*.spec.ts'],
        preprocessors: {
            '**/*.spec.ts': ['webpack', 'sourcemap']
        },
        webpack: webpackConfig,
        reporters: [
            'spec'
        ],
        browsers: [
            'Firefox'
        ]
    });
};