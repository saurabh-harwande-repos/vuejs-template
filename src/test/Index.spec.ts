import { shallowMount } from '@vue/test-utils';
import Index from '../pages/Index.vue';
import { expect } from 'chai';

describe('Index.vue', () => {
    test('renders props.msg when passed', () => {
        const msg = 'Hello, Saurabh!!!';
        const wrapper = shallowMount(Index);
        expect(wrapper.find('div').text()).equals(msg);
    });
});